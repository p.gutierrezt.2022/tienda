#!/usr/bin/env pythoh3

'''
Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

articulos = {}

def anadir(articulo, precio):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo]=precio
    return
def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""

    for articulo,precio in articulos.items():
        print(articulo+":",float(precio),"euros")

def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar.

    Muestra un mensaje pidiendo el artículo a comprar.
    Cuando el usuario lo escribe, comprueba que es un artículo qie
    está en el diccionario artículos, y termina devolviendo ese
    artículo como string. Si el artículo escrito por el usuario no
    está en artículos, pide otro hasta que escriba uno que sí está."""

    articulo=input("Artículo: ")
    while articulo not in articulos:
        articulo=input("Artículo: ")

    return articulo

def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar.

    Muestra un mensaje pidiendo la cantidad a comprar.
    Cuando el usuario lo escribe, comprueba que es un número real
    (float), y termina devolviendo esa cantidad.. Si la cantidad
    escrita no es un float, pide otra hasta que escriba una correcta.
    """
    while True:
        cantidad = input("Cantidad: ")
        try:
            cantidad = float(cantidad)
            print()
            break
        except ValueError:
            pass
    return cantidad
def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, *****!! almacénalos en el diccionario artículos mediante
    llamadas a la función añadir !!*****. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """

    dicc=sys.argv.pop(0)
    try:
        for i in range(0,len(sys.argv),2):
            articulo=sys.argv[i]
            precio=sys.argv[i+1]
            precio_float=float(precio)
            anadir(articulo, precio)
            bool1=True

    except IndexError:
        print("Error en argumentos: Hay al menos un artículo sin precio:",articulo+".")
        bool1 = False
        sys.exit(1)

    except ValueError:
        print("Error en argumentos:", precio ,"no es un precio correcto.")
        bool1=False

    try:
        if bool1==True:
            print("Lista de artículos en la tienda:")
            mostrar()
            print()
            valor_art = pedir_articulo()
            valor_cant = pedir_cantidad()
            A = float(articulos[valor_art])
            print("Compra total:",valor_cant,"de",valor_art+", a pagar", A * float(valor_cant))

    except UnboundLocalError:
        print("Error en argumentos: no se han especificado artículos.")
        bool1 = False
        sys.exit(1)

if __name__ == '__main__':
    main()